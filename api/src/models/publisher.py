from django.db import models
from django.db.models import ( 
    ImageField, 
    CharField, 
    ForeignKey,
    )
from src.models.hero import Hero 
from src.models.comic import Comic 

#My Model 

class Publisher(models.Model):
    name = models.CharField(max_length=100)
    founder = models.CharField(max_length=100)
    hero = models.ForeignKey(Hero, related_name="hero", on_delete=models.CASCADE)
    comicn = models.ForeignKey(Comic, related_name="comicn", on_delete=models.CASCADE)
    description = models.TextField() 

    class Meta:
        ordering = ('name',)
    
    def __unicode__(self):
        return self.name