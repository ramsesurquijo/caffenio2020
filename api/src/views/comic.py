from rest_framework.generics import (
    ListAPIView,
    #En caso de querer crear utilizar el siguiente
    ListCreateAPIView ,
    RetrieveUpdateDestroyAPIView
)
from src.models.comic import Comic
from src.serializers.comic import ComicSerializer, ComicDetailSerializer

class ComicList(ListCreateAPIView):
    queryset = Comic.objects.all()
    serializer_class = ComicSerializer
    

class ComicDetail(RetrieveUpdateDestroyAPIView):
    queryset = Comic.objects.all()
    serializer_class = ComicDetailSerializer