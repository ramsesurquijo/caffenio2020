from rest_framework.generics import (
    ListAPIView,
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView
)
from src.models.hero import Hero
from src.serializers.hero import HeroSerializer

class HeroList(ListCreateAPIView):
    queryset = Hero.objects.all()
    serializer_class = HeroSerializer

class HeroDetail(RetrieveUpdateDestroyAPIView):
    queryset = Hero.objects.all()
    serializer_class = HeroSerializer
    # lookup_field = 'id'  