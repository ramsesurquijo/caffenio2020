# # """Circles URLs."""

# # Django
# from django.urls import include, path

# # Django REST Framework
# from rest_framework.routers import DefaultRouter

# # Views
# from src.views import views as hero_views
# from .views import memberships as membership_views

# router = DefaultRouter()
# router.register(r'views', hero_views.HeroDetail, basename='circle')
# router.register(
#     r'circles/(?P<slug_name>[-a-zA-Z0-0_]+)/members',
#     membership_views.MembershipViewSet,
#     basename='membership'
# )

# urlpatterns = [
#     path('', include(router.urls))
# ]