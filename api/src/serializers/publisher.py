from rest_framework.serializers import (
    SerializerMethodField,
    CharField,
    ModelSerializer,
    ReadOnlyField
)
from src.models.publisher import Publisher
from src.models.hero import Hero
from src.models.comic import Comic

class PublisherSerializer(ModelSerializer):
    heroRealName =  ReadOnlyField(source='hero.name')
    comicname = CharField(read_only=True, source='comicn.comicName')
    class Meta:
        model = Publisher
        fields = ('id', 'name', 'founder', 'comicname', 'heroRealName', 'description')
    


class PublisherDetailSerializer(ModelSerializer):
    class Meta:
        model = Publisher
        fields = ('id', 'name', 'founder', 'hero', 'comicn', 'description')
       

