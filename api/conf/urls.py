"""conf URL
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls import include, url

#import views
from src.views.comic import ComicDetail, ComicList
from src.views.hero import HeroDetail, HeroList
from src.views.publisher import PublisherDetail, PublisherList


urlpatterns = [
    url('admin/', admin.site.urls),
    #home
    path('', HeroList.as_view(),name='heroList'),
    #urls for hero
    path('api/hero/', HeroList.as_view(),name='heroList'),
    path('api/hero/<int:pk>', HeroDetail.as_view(),name='heroDetail'),
    #urls for comics
    path('api/comic/', ComicList.as_view(),name='comicList'),
    path('api/comic/<int:pk>', ComicDetail.as_view(),name='comicDetail'),
    #urls for Publisher
    path('api/publisher/', PublisherList.as_view(),name='comicList'),
    path('api/publisher/<int:pk>', PublisherDetail.as_view(),name='comicDetail'),


]
